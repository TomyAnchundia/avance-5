const { Router } = require('express');
// const { deleteEntrega } = require('../controllers/entregar-laboratorio.controller');
const { getAllReserva, getReserva, createReserva, deleteReserva, updateReserva } = require('../controllers/reserva-laboratorio.controller')

const router = Router();

router.get('/reserva', getAllReserva)
router.get('/reserva/:id_reservar_laboratorio', getReserva)
router.post('/reserva', createReserva)
router.delete('/reserva/:id_reservar_laboratorio', deleteReserva)
router.put('/reserva/:id_reservar_laboratorio', updateReserva)


module.exports = router;