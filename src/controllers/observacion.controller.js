const pool = require('../db');

const getAllObservacion = async (req, res, next) => {
    try {
        const allOb = await pool.query('SELECT * FROM observacion')

        res.json(allOb.rows)

    } catch (error) {
        next(error);
    }
}
const getObservacion = async (req, res, next) => {
    res.send('Retrieving a single Observacion');
}
const createObservacion = async (req, res, next) => {
    const { 
        id_docente,
        descripcion_recomendacion,
        descripcion_observacion,
        
    } = req.body;

    try {
        const tiempoTranscurrido = Date.now();
        const fecha_observacion= new Date(tiempoTranscurrido);
        const result = await pool.query("INSERT INTO observacion ( id_docente, fecha_observacion, descripcion_recomendacion, descripcion_observacion) VALUES ($1, $2, $3, $4) RETURNING *",
            [
                id_docente,
                fecha_observacion,
                descripcion_recomendacion,
                descripcion_observacion,
            ]
        );
        
        
        if(result.rowCount == 0){
            res.json("observacion no guardada")
        }else if(result.rowCount >= 1){
            res.json("observacion guardada")
        }
    } catch (error) {
        next(error);
    }
}
const deleteObservacion = async (req, res, next) => {
    res.send('Deleting Observacion');
}
const updateObservacion = async (req, res, next) => {
    res.send('Updating observacion');
}

module.exports = {
    getAllObservacion,
    getObservacion,
    createObservacion,
    deleteObservacion,
    updateObservacion
}