const pool = require("../db");
const getAllReserva = async (req, res, next) => {
  try {
    const allOb = await pool.query(
      "SELECT * FROM reservar_laboratorio " +
        "inner join detalle_reserva on detalle_reserva.id_detalle_reserva = reservar_laboratorio.id_detalle_reserva"
    );

    res.json(allOb.rows);
  } catch (error) {
    next(error);
  }
};
const getReserva = async (req, res, next) => {
  res.send("Retrieving a single reserva");
};
const createReserva = async (req, res, next) => {
  const {
    id_docente,
    fecha_reserva,
    hora_inicio,
    hora_fin,
    id_materia,
    id_laboratorio,
    numero_maquinas_usar,
  } = req.body;
  try {
    const result = await pool.query(
      "INSERT INTO detalle_reserva ( numero_maquinas_usar, fecha_reserva, id_materia, hora_inicio, hora_fin) VALUES ($1, $2, $3, $4, $5) RETURNING *",
      [numero_maquinas_usar, fecha_reserva, id_materia, hora_inicio, hora_fin]
    );
    const { id_detalle_reserva } = result.rows[0];
    try {
      const busqueda = await pool.query(
        "INSERT INTO reservar_laboratorio (id_detalle_reserva,id_laboratorio, id_docente) VALUES ($1, $2, $3) RETURNING *",
        [id_detalle_reserva, id_laboratorio, id_docente]
      );

      if (busqueda.rowCount == 0) {
        res.json("reserva no guardada");
      } else if (busqueda.rowCount >= 1) {
        res.json("reserva guardada");
      }
      // res.json(busqueda.rows)
    } catch (error) {
      next(error);
    }

    // res.json(result.rows[0]);
  } catch (error) {
    next(error);
  }
};
const deleteReserva = async (req, res, next) => {
  const {id_reservar_laboratorio} = req.params;
  try {
    const result = await pool.query(
      "DELETE FROM reservar_laboratorio WHERE id_reservar_laboratorio = $1 RETURNING*",
      [id_reservar_laboratorio]
    );
    if(result.rowCount === 0)
    return res.status(404).json({
      message: "Reserva de laboratorio no encontrada",
    });
    return res.SendStatus(204);
  } catch (error) {
    next(error);
  }
};
const updateReserva = async (req, res, next) => {
  try {
    const {id_detalle_reserva} = req.params;
    const {id_laboratorio, id_docente, id_materia, numero_maquinas_usar, fecha_reserva, hora_inicio, hora_fin} = 
    req.body;

    const result = await pool.query(
      "UPDATE reservar_laboratorio SET id_laboratorio = $1, id_docente = $2, id_materia = $3, numero_maquinas_usar = $4, fecha_reserva = $5, hora_inicio = $6, hora_fin = $7 RETURNING*",
      [id_laboratorio, id_docente, id_materia, numero_maquinas_usar, fecha_reserva, hora_inicio, hora_fin]
    );
    if (result.rows.length === 0)
      return res.status(404).json({
        message: "Reserva no encontrada",

      });
      return res.json(result.rows[0]);
  } catch (error) {
    next(error);
  }
};

module.exports = {
  getAllReserva,
  getReserva,
  createReserva,
  deleteReserva,
  updateReserva,
};
